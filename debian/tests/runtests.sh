#!/bin/sh

set -e

theHarvester -d kali.org -b urlscan >results

if ! grep -q "\[\*\] Hosts found" results; then
    echo "error: no hosts found"
    exit 1
fi
